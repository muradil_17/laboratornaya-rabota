import React, {Component} from 'react';
import './App.css';
import Card from "./componenty/card";

class App extends Component {
    state = {
      suit : [
          "♥",
          "♣",
          "♠",
          "♦",
        ]
    };

    rank = {
      rankcards:[
          "a",
          "k",
          "q",
          "j",
          "10",
          "9",
          "8",
          "7",
          "6"
        ]
    };

    img = {
      imgcards : [
          "hearts",
          "clubs",
          "spades",
          "diams"
        ]
    };

    changecards = () => {
        //здесь должна быть цикл?
    };


    render() {
        return (
            <div className="App">
                <button onClick={this.changecards}>new cards</button>
                <Card icon={this.state.suit[0]} rank={this.rank.rankcards[8]} suit={this.img.imgcards[0]}/>
                <Card icon={this.state.suit[1]} rank={this.rank.rankcards[0]} suit={this.img.imgcards[2]}/>
                <Card icon={this.state.suit[2]} rank={this.rank.rankcards[0]} suit={this.img.imgcards[3]}/>
                <Card icon={this.state.suit[3]} rank={this.rank.rankcards[0]} suit={this.img.imgcards[1]}/>
                <Card icon={this.state.suit[2]} rank={this.rank.rankcards[1]} suit={this.img.imgcards[2]}/>
            </div>
        );
    }
}
export default App;
